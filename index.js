/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:


	function getUserInfo() {
  	let fullName = prompt("What is your fullname?");
  	let age = prompt("How old are you?");
  	let location = prompt("Where are you located?");

  	console.log("Hello, " + fullName);
  	console.log("You are " + age + " years old.");
  	console.log("You live in " + location + ".");
}


getUserInfo();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

		// let band1 = "Sabrina Carpenter";
		// let band2 = "Harry Styles";
		// let band3 = "One Direction";
		// let band4 = "BIG BANG";
		// let band5 = "Kanye";

		// console.log("1. " + band1);
		// console.log("2. " + band2);
		// console.log("3. " + band3);
		// console.log("4. " + band4);
		// console.log("5. " + band5);

	function displayFavoriteBands() {
	  console.log("My top 5 favorite bands/musical artists:");
	  console.log("1. Sabrina Carpenter");
	  console.log("2. Harry Styles");
	  console.log("3. One Direction");
	  console.log("4. BIG BANG");
	  console.log("5. Kanye");
	}

	displayFavoriteBands();

	



/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function displayFavoriteMovies() {
	let rate1 = "85%";
	console.log("1. Avengers Infinity War");
	console.log("Rotten Tomatoes Rating: " + rate1);

	let rate2 = "94%";
	console.log("2. Call Me By Your Name");
	console.log("Rotten Tomatoes Rating: " + rate2);

	let rate3 = "99%";
	console.log("3. Parasite");
	console.log("Rotten Tomatoes Rating: " + rate3);


	let rate4 = "94%";
	console.log("4. Avengers End Game");
	console.log("Rotten Tomatoes Rating: " + rate4);

	let rate5 = "93%";
	console.log("5. Spiderman No Way Home");
	console.log("Rotten Tomatoes Rating: " + rate5);


}

displayFavoriteMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// 

// let printFriends() = 
	function printUsers() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printUsers();

// console.log(friend1);
// console.log(friend2);

